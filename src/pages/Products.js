import { Fragment, useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';


export default function Products() {

	//State tha will be used to store the products retrieved form the database
	const [products, setProducts] = useState([]);

	const { user } = useContext(UserContext);

	//Retrieve the products from the database upon initial render of the product component.
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}products/`)
			.then(res => res.json())
			.then(data => {

				console.log(data);

				setProducts(data.map(product => {
					return (
						<ProductCard key={product._id} productProp={product} />
					)
				}))

			})
	}, [])

	return (
		(user.isAdmin)
			?
			<Navigate to="/admin" />
			:
			<Fragment>
				<h1 className="text-center my-3">Products</h1>
				{products}
			</Fragment>
	)
}
