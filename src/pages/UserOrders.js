import { useContext, useState, useEffect } from "react";
import {Table} from "react-bootstrap";
import {Navigate, useParams} from "react-router-dom";
import UserContext from "../UserContext";



export default function UserOrders(){

	// to validate the user role.
	const {user} = useContext(UserContext);

	//Create allProducts State to contain the products from the database.
	const [userOrders, setUserOrders] = useState([]);

	const { userId } = useParams();

	//"fetchData()" wherein we can invoke if their is a certain change with the product.
	const  fetchData = async () =>{
		// Get all courses in the database
		await fetch(`${process.env.REACT_APP_API_URL}${userId}/myOrders`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})

       
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUserOrders(data.map(order => {
				return(
					<tr key={order._id}>
						<td>{order.userId}</td>
						<td>{order.productId}</td>
                        <td>{order.productPrice}</td>
						
					</tr>
				)
			}))

		})
	}


	

	// To fetch all products in the first render of the page.
	useEffect(()=>{
	// 	// invoke fetchData() to get all courses.
		fetchData();
        console.log(fetchData);
	}, [])

    

	return(
		(user.isAdmin === false)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>My Orders</h1>
				{/*A button to add a new course*/}
				
			</div>
			<Table responsive="lg" variant="primary" striped bordered hover>
		     <thead>
		       <tr>
		         
		         <th>User ID</th>
                 <th>Product ID</th>
		         <th>Product Price</th>
		         
		        
		       </tr>
		     </thead>
		     <tbody>
		       { userOrders }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/userOrders" />
	)
}