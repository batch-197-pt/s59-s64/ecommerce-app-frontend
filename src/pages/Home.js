import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Carousel from '../components/Carousel';
import Featured from '../components/Featured';



export default function Home() {

	const data = {
		title: "gitPC",
		content: "The place to build your rig.",
		destination: "/products",
		label: "Build now!"
	}

	return (
		<Fragment>
			<Banner data={data} />
			<Highlights />
			<Carousel />
			<Featured />
			
		</Fragment>
	)
};