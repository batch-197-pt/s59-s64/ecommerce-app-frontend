import { useState, useEffect } from 'react';
import { Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({ productProp }) {

  const { name, description, price, _id } = productProp;


  const [count, setCount] = useState(0);
  const [stocks, setStocks] = useState(10);


  function order() {
    if (stocks > 0) {
      setCount(count + 1)
      console.log('Orders: ' + count)
      setStocks(stocks - 1)
      console.log('Stocks: ' + stocks)
    }
  };


  useEffect(() => {
    if (stocks === 0) {
      alert("Product out of stock.")
    }
  }, [stocks])

  return (

    <Row>
      <Col lg={{ span: 6, offset: 3 }}>
        <Card className="productCard my-3">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            <Button className="bg-primary" as={Link} to={`/products/${_id}`} >Details</Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>

  )
}