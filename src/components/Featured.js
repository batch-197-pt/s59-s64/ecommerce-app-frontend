import { Row, Col, Card, Button } from 'react-bootstrap'



function Featured() {

    return (

        <Row>
            <Col xs={12} md={4}>
                <Card style={{ width: '18rem' }}>
                    <Card.Body>
                        <Card.Img variant="top" src={require("../images/x1.jpg")} />
                        <Card.Title>Ryzen 9 5900x</Card.Title>
                        
                        <Card.Text>
                            A blazingly fast processor that boasts a 12 cores/24 threads to ace your next competitive game.
                        </Card.Text>
                        <Card.Link href="/products/6347f605529196e445461079">Read More</Card.Link>
                        
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card style={{ width: '18rem' }}>
                    <Card.Body>
                        <Card.Img variant="top" src={require("../images/x2.jpg")} />
                        <Card.Title>Sapphire Pulse 5600XT</Card.Title>
                    
                        <Card.Text>
                            Our best-selling bang for one's buck GPU! 
                        </Card.Text>
                        <Card.Link href="/products/634d42321da2a43a5f5a7403">Read More</Card.Link>
                        
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card style={{ width: '18rem' }}>
                    <Card.Body>
                        <Card.Img variant="top" src={require("../images/x4.jpg")} />
                        <Card.Title>Asus ROG Strix Scope TKL Deluxe</Card.Title>
                        <Card.Text>
                        Mkb mech keyboard, cherry mx red switch
                        </Card.Text>
                        <Card.Link href="/products/634d42cb1da2a43a5f5a744b">Read More</Card.Link>
                        
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}

export default Featured;

