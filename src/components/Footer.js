import { CDBFooter, CDBFooterLink, CDBBox, CDBBtn, CDBIcon } from 'cdbreact';

export default function Footer(){
  return (
    <CDBFooter id="myFooter" className="shadow">
      <CDBBox display="flex" flex="column" className="mx-auto py-5" style={{ width: '90%' }}>
        <CDBBox display="flex" justifyContent="between" className="flex-wrap">
          <CDBBox>
            <a href="/" className="d-flex align-items-center p-0 text-dark">
              
              <span className="ml-3 h5 font-weight-bold">gitPC</span>
            </a>
            <p className="my-3" style={{ width: '250px' }}>
              We sell the best quality hardware for your next build of rig.
            </p>
            <CDBBox display="flex" className="mt-4">
              <CDBBtn flat color="dark" href="https://www.facebook.com/" target="_blank">
                <CDBIcon fab icon="facebook-f"  />
              </CDBBtn>
              <CDBBtn flat color="dark" className="mx-3" href="https://www.twitter.com/" target="_blank">
                <CDBIcon fab icon="twitter" />
              </CDBBtn>
              <CDBBtn flat color="dark" className="p-2" href="https://www.instagram.com/" target="_blank">
                <CDBIcon fab icon="instagram" />
              </CDBBtn>
            </CDBBox>
          </CDBBox>
          <CDBBox>
            <p className="h5 mb-4" style={{ fontWeight: '600' }}>
              gitPC
            </p>
            <CDBBox flex="column" style={{ cursor: 'pointer', padding: '0' }}>
              <CDBFooterLink href="/">Resources</CDBFooterLink>
              <CDBFooterLink href="/">About Us</CDBFooterLink>
              {/* <CDBFooterLink href="/">Contact</CDBFooterLink>
              <CDBFooterLink href="/">Blog</CDBFooterLink> */}
            </CDBBox>
          </CDBBox>
          <CDBBox>
            <p className="h5 mb-4" style={{ fontWeight: '600' }}>
              Help
            </p>
            <CDBBox flex="column" style={{ cursor: 'pointer', padding: '0' }}>
              <CDBFooterLink href="/register">Sign Up</CDBFooterLink>
              <CDBFooterLink href="/login">Sign In</CDBFooterLink>
            </CDBBox>
          </CDBBox>
          <CDBBox>
            <p className="h5 mb-4" style={{ fontWeight: '600' }}>
              Products
            </p>
            <CDBBox flex="column" style={{ cursor: 'pointer', padding: '0' }}>
              <CDBFooterLink href="/products">Processor</CDBFooterLink>
              <CDBFooterLink href="/products">Motherboard</CDBFooterLink>
              <CDBFooterLink href="/products">Graphics Card</CDBFooterLink>
            </CDBBox>
          </CDBBox>
        </CDBBox>
        <small className="text-center mt-5">&copy; gitPC, 2022. All rights reserved.</small>
      </CDBBox>
    </CDBFooter>
  );
};