import { Carousel } from "react-bootstrap";
// import images from "../images/";


export default function homeCarousel() {
    return (
        <Carousel fade>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={require("../images/y1.jpg")}
              alt="First slide"
            />
            <Carousel.Caption>
              <h3>AMD Processors</h3>
              <p>We have the latest AMD Products</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={require("../images/y2.jpg")}
              alt="Second slide"
            />
    
            <Carousel.Caption>
              <h3>Intel Processors</h3>
              <p>We have the latest Intel Products</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={require("../images/y3.jpg")}
              alt="Third slide"
            />
    
            <Carousel.Caption>
              <h3>Game Face On!</h3>
              <p>
                Start building your PC! Add RGB lights to your rig to experience additional 5 FPS in games.
              </p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      );
}