import { Row, Col, Card } from 'react-bootstrap';


export default function Highlights() {
	return (

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>We deliver nationwide</Card.Title>
						<Card.Text>
							From Metro Manila and anywhere else in the philippines.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>Only the good quality</Card.Title>
						<Card.Text>
							We only sell the best quality pieces of hardware. <br></br> <br></br>	For real.
						</Card.Text>
					</Card.Body>
				</Card>

			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>Gaming build, productivity build, and online class build</Card.Title>
						<Card.Text>
							Wether you are building your PC for the first time or a PC enthusiast, we have it all for you.
						</Card.Text>
					</Card.Body>
				</Card>

			</Col>
		</Row>

	)
};