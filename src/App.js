import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';

// COMPONENTS
import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';

// PAGES
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import AdminDashboard from './pages/AdminDashboard';
import AddProduct from './pages/AddProduct';
import EditProduct from './pages/EditProduct';
import AdminOrders from './pages/AdminOrders'
import UserOrders from './pages/UserOrders'
import Error from './pages/Error';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
// import Account from './pages/Account';


// import logo from './logo.svg';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }


  // This side effect is triggered by once the user signs in
  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: { //get the token from the localstorage to access user details
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {

        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {

          setUser({
            id: null,
            isAdmin: null
          })

        }
      })

  }, []);


  return (

    <UserProvider value={{ user, setUser, unsetUser }} >
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            {/* <Route path="/account" element={<Account />} /> */}
            <Route path="/register" element={<Register />} />
            <Route path="/admin" element={<AdminDashboard />} />
            <Route path="/addProduct" element={<AddProduct />} />
            <Route path="/adminOrders" element={<AdminOrders />} />
            <Route path="/userOrders" element={<UserOrders />} />
            <Route path="/editProduct/:productId" element={<EditProduct />} />
            <Route path="/products" element={<Products />} />
            <Route path="/products/:productId" element={<ProductView />} />
            <Route path="*" element={<Error/>} />
          </Routes>
        </Container>
        <Footer />
      </Router>
    </UserProvider>

  );
}

export default App;

